package pl.pgebala.testcontainers.service;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pgebala.testcontainers.document.Item;
import pl.pgebala.testcontainers.entity.ItemEntity;
import pl.pgebala.testcontainers.repository.EsItemRepository;
import pl.pgebala.testcontainers.repository.JpaItemRepository;

@Service
@RequiredArgsConstructor
public class ItemService {

    private final EsItemRepository esItemRepository;

    private final JpaItemRepository jpaItemRepository;

    public Item save(Item item) {
        return esItemRepository.save(item);
    }

    public List<Item> findAll() {
        List<Item> items = new ArrayList<>();
        esItemRepository.findAll().forEach(items::add);
        return items;
    }

    @Transactional
    public ItemEntity saveEntity(ItemEntity item) {
        return jpaItemRepository.save(item);
    }

    @Transactional(readOnly = true)
    public List<ItemEntity> findAllEntity() {
        return jpaItemRepository.findAll();
    }

    @Transactional
    public void deleteEntityByIds(List<Long> ids) {
        jpaItemRepository.deleteAllById(ids);
    }
}
