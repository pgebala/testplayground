package pl.pgebala.testcontainers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pgebala.testcontainers.entity.ItemEntity;

public interface JpaItemRepository extends JpaRepository<ItemEntity, Long> {

}
