package pl.pgebala.testcontainers.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import pl.pgebala.testcontainers.document.Item;

public interface EsItemRepository extends ElasticsearchRepository<Item, String> {

}
