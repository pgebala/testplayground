package pl.pgebala.testcontainers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories
public class TestContainersPlaygroundApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestContainersPlaygroundApplication.class, args);
    }
}
