package pl.pgebala.testcontainers.service;

import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.pgebala.testcontainers.entity.ItemEntity;
import pl.pgebala.testcontainers.repository.EsItemRepository;

@SpringBootTest
@Testcontainers
public class JpaServiceTest {

    @Container
    public static MySQLContainer container = new MySQLContainer()
        .withUsername("username")
        .withPassword("password")
        .withDatabaseName("ITEM_TEST_DATABASE");

    @Autowired
    private ItemService itemService;

    @BeforeAll
    public static void setUp() {
        container.start();
        System.setProperty("spring.datasource.url", container.getJdbcUrl());
        System.setProperty("spring.datasource.driver-class-name", container.getDriverClassName());
    }

    @Test
    public void shouldDatabaseRunningAndNamed() {
        Assert.assertTrue(container.isRunning());
        Assert.assertEquals(container.getDatabaseName(), "ITEM_TEST_DATABASE");
    }

    @Test
    public void shouldExistOneItemEntityInDatabaseAfterSave() {
        itemService.saveEntity(ItemEntity.builder().name("item1").build());
        Assert.assertEquals(itemService.findAllEntity().size(), 1);
    }

    @Test
    public void shouldNoExistItemEntityInDatabaseAfterSaveAndDelete() {
        itemService.saveEntity(ItemEntity.builder().name("item2").build());
        List<Long> idsToDelete = itemService.findAllEntity().stream().map(ItemEntity::getId).collect(Collectors.toList());
        itemService.deleteEntityByIds(idsToDelete);
        Assert.assertEquals(itemService.findAllEntity().size(), 0);
    }

    @MockBean
    private EsItemRepository esItemRepository;
}
