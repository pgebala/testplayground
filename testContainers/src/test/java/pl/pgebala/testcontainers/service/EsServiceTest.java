package pl.pgebala.testcontainers.service;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.Container.ExecResult;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.pgebala.testcontainers.document.Item;
import pl.pgebala.testcontainers.repository.EsItemRepository;
import pl.pgebala.testcontainers.repository.JpaItemRepository;

@Slf4j
@SpringBootTest
@Testcontainers
public class EsServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(EsServiceTest.class);

    private static final String ES_DOCKER_IMAGE_NAME = "docker.elastic.co/elasticsearch/elasticsearch:7.12.1";

    @Container
    public static ElasticsearchContainer container = new ElasticsearchContainer(ES_DOCKER_IMAGE_NAME)
        .withClasspathResourceMapping(
            "file.txt",
            "/etc/file.txt",
            BindMode.READ_ONLY
        )
        .withStartupTimeout(Duration.of(60, ChronoUnit.SECONDS))
        .waitingFor(
            Wait.forListeningPort()
        );
    ;

    @Autowired
    private ItemService esService;

    @Autowired
    private EsItemRepository esItemRepository;

    @BeforeAll
    public static void setUp() {
        container.start();
        Slf4jLogConsumer logConsumer = new Slf4jLogConsumer(LOGGER);
        container.followOutput(logConsumer);
        System.setProperty("spring.elasticsearch.rest.uris", container.getContainerIpAddress() + ":" + container.getMappedPort(9200));
    }

    @AfterEach
    public void afterEach() {
        esItemRepository.deleteAll();
    }

    @Test
    public void execInContainerTest() throws IOException, InterruptedException {
        ExecResult lsResult = container.execInContainer("ls", "-la", "/");
        String stdout = lsResult.getStdout();
        int exitCode = lsResult.getExitCode();
        log.info("execInContainerTestResult: \n {}", stdout);
        Assert.assertTrue(stdout.contains(".dockerenv"));
        Assert.assertEquals(exitCode, 0);
    }

    @Test
    public void fileMappingTest() throws IOException, InterruptedException {
        ExecResult catResult = container.execInContainer("cat", "/etc/file.txt");
        String stdout = catResult.getStdout();
        Assert.assertEquals(stdout, "file content\n");
    }

    @Test
    public void shouldExistOneItemInEsAfterSave() {
        esService.save(new Item("1", "item11"));
        Assert.assertEquals(esService.findAll().size(), 1);
    }

    @Test
    public void shouldExistFourItemInEsAfterSave() {
        esService.save(new Item("2", "item22"));
        esService.save(new Item("3", "item33"));
        esService.save(new Item("4", "item44"));
        esService.save(new Item("5", "item55"));
        Assert.assertEquals(esService.findAll().size(), 4);
    }

    @Test
    public void shouldNoExistItemInEs() {
        Assert.assertEquals(esService.findAll().size(), 0);
    }


    @MockBean
    private JpaItemRepository jpaItemRepository;

    @MockBean
    private DataSource dataSource;

    @MockBean
    private EntityManagerFactory entityManagerFactory;
}
