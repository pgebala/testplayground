package pl.pgebala.archunit;

import static com.tngtech.archunit.core.domain.JavaClass.Predicates.resideInAPackage;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaAccess;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;
import com.tngtech.archunit.library.GeneralCodingRules;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@AnalyzeClasses(
    packages = "pl.pgebala.archunit",
    importOptions = {
        ImportOption.DoNotIncludeTests.class,
        ImportOption.DoNotIncludeJars.class,
    }
)
class ArchUnitApplicationTests {

    @ArchTest
    static void coreApiTestExample(JavaClasses classes) {
        Set<JavaClass> services = new HashSet<>();
        for (JavaClass clazz : classes) {
            if (clazz.getName().contains(".service.")) {
                services.add(clazz);
            }
        }
        for (JavaClass service : services) {
            for (JavaAccess<?> access : service.getAccessesFromSelf()) {
                String targetName = access.getTargetOwner().getName();

                if (targetName.contains(".entity.")) {
                    String message = String.format(
                        "Service %s accesses Entity %s in line %d",
                        service.getName(), targetName, access.getLineNumber()
                    );

                    throw new AssertionError(message);
                }
            }
        }
    }

    @ArchTest
    static void langApiTestExample(JavaClasses classes) {
        noClasses()
            .that().resideInAPackage("..service..")
            .should().accessClassesThat().resideInAPackage("..entity..")
            .check(classes);
    }

    // Package Dependency

    @ArchTest
    static void servicePackageShouldBeAccessedOnlyFromControllerAndServiceAndUtilPackages(JavaClasses classes) {
        classes()
            .that().resideInAPackage("..service..")
            .should().onlyBeAccessed().byAnyPackage("..controller..", "..service..")
            .check(classes);
    }

    @ArchTest
    static void utilPackageShouldOnlyHaveDependentClassesInServicePackage(JavaClasses classes) {
        classes().that().resideInAPackage("..util..")
            .should().onlyHaveDependentClassesThat().resideInAnyPackage("..service..")
            .check(classes);
    }

    // Class and Package Containment Checks

    @ArchTest
    static void controllerNamedClassShouldBeInControllerPackage(JavaClasses classes) {
        classes().that().haveSimpleNameEndingWith("Controller")
            .should().resideInAPackage("..controller..")
            .check(classes);
    }

    @ArchTest
    static void serviceNamedClassShouldBeInServicePackage(JavaClasses classes) {
        classes().that().haveSimpleNameEndingWith("Service")
            .should().resideInAPackage("..service..")
            .check(classes);
    }

    @ArchTest
    static void serviceImplNamedClassShouldBeInServiceImplPackage(JavaClasses classes) {
        classes().that().haveSimpleNameEndingWith("ServiceImpl")
            .should().resideInAPackage("..service.impl..")
            .check(classes);
    }

    @ArchTest
    static void classInServicePackageShouldBeInterfaces(JavaClasses classes) {
        classes().that().resideInAPackage("..service")
            .should().beInterfaces()
            .check(classes);
    }

    @ArchTest
    static void classInServiceImplPackageShouldBeImplementsServiceClass(JavaClasses classes) {
        classes().that().resideInAPackage("..service.impl")
            .should().implement(resideInAPackage("..service"))
            .check(classes);
    }

    @ArchTest
    static void jpaRepositoryClassShouldBeNamedRepositoryAndShouldBeInRepositoryPackage(JavaClasses classes) {
        classes().that().implement(JpaRepository.class)
            .should().haveSimpleNameEndingWith("Repository")
            .andShould().resideInAPackage("..repository..")
            .check(classes);
    }

    @ArchTest
    static void jpaRepositoryClassShouldBeNamedRepositoryAndShouldBeInRepositoryPackage2(JavaClasses classes) {
        methods().that().areDeclaredInClassesThat(resideInAPackage("..mapper.."))
            .should().beStatic()
            .check(classes);
    }

    // Annotation Checks

    @ArchTest
    static void serviceImplClassShouldBeAnnotatedWithService(JavaClasses classes) {
        classes().that().haveSimpleNameEndingWith("ServiceImpl")
            .should().beAnnotatedWith(Service.class)
            .check(classes);
    }

    @ArchTest
    static void controllerAnnotatedClassShouldBeNamedWithController(JavaClasses classes) {
        classes().that().areAnnotatedWith(Controller.class)
            .should().haveSimpleNameEndingWith("Controller")
            .check(classes);
    }

    // Layer Checks

    @ArchTest
    static void specificLayersShouldOnlyBeAccessedBnSpecificLayers(JavaClasses classes) {
        layeredArchitecture()
            .layer("Controller").definedBy("..controller..")
            .layer("Service").definedBy("..service..")
            .layer("Util").definedBy("..util..")
            .layer("Dto").definedBy("..dto..")
            .layer("Mapper").definedBy("..mapper..")
            .layer("Repository").definedBy("..repository..")
            .layer("Entity").definedBy("..entity..")

            .whereLayer("Controller").mayNotBeAccessedByAnyLayer()
            .whereLayer("Service").mayOnlyBeAccessedByLayers("Controller")
            .whereLayer("Util").mayOnlyBeAccessedByLayers("Service")
            .whereLayer("Dto").mayOnlyBeAccessedByLayers("Controller", "Service", "Mapper", "Util")
            .whereLayer("Mapper").mayOnlyBeAccessedByLayers("Service", "Util")
            .whereLayer("Repository").mayOnlyBeAccessedByLayers("Service")
            .whereLayer("Entity").mayOnlyBeAccessedByLayers("Repository", "Mapper")

            .because("Bo tak!!!")
            .check(classes);
    }

    @ArchTest
    static void methodTest(JavaClasses classes) {
        methods()
            .that().arePublic()
            .and().areDeclaredInClassesThat().resideInAPackage("..service.impl..")
            .should().beAnnotatedWith(Transactional.class)
            .check(classes);
    }

    @ArchTest
    static void customRule(JavaClasses classes) {
        DescribedPredicate<JavaClass> isAnnotatedWithService =
            new DescribedPredicate<JavaClass>("have a field annotated with @Service") {
                @Override
                public boolean apply(JavaClass input) {
                    return input.isAnnotatedWith("org.springframework.stereotype.Service");
                }
            };

        ArchCondition<JavaClass> mustHaveSaveMethod =
            new ArchCondition<JavaClass>("must have 'save' method") {
                @Override
                public void check(JavaClass item, ConditionEvents events) {
                    Optional<JavaMethod> method = item.getAllMethods().stream().filter(m -> m.getName().equals("save")).findFirst();
                    if (method.isEmpty()) {
                        String message = String.format(
                            "Class %s not contain 'save' method", item.getName());
                        events.add(SimpleConditionEvent.violated(item, message));
                    }
                }
            };

        classes()
            .that(isAnnotatedWithService)
            .should(mustHaveSaveMethod)
            .check(classes);
    }

    @ArchTest
    static void generalCodingRules(JavaClasses classes) {
        GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS
            .check(classes);

        GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION
            .check(classes);
    }
}
