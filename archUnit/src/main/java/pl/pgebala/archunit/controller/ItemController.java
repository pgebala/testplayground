package pl.pgebala.archunit.controller;

import java.util.List;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.pgebala.archunit.dto.ItemDto;
import pl.pgebala.archunit.entity.Item;
import pl.pgebala.archunit.service.ItemService;

@RestController
@RequestMapping("/api/item")
@RequiredArgsConstructor
public class ItemController {

    private final ItemService itemService;

    @GetMapping
    public List<ItemDto> findAll() {
        return itemService.findAll();
    }

    @GetMapping("/{id}")
    public ItemDto findOne(@PathVariable Long id) throws Exception {
        if (id == null) {
            throw new NotFoundException("nie ma");
        }
        return itemService.findOne(id);
    }
}
