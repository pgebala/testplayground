package pl.pgebala.archunit.util;

import java.util.List;
import org.springframework.stereotype.Component;
import pl.pgebala.archunit.dto.ItemDto;

@Component
public class ItemUtil {

    public long count(List<ItemDto> items) {
        return items.size();
    }
}
