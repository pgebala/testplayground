package pl.pgebala.archunit.repository;

import org.springframework.stereotype.Repository;
import pl.pgebala.archunit.entity.Item;

@Repository
public class CustomItemRepository {

    public Item getItem(Long id) {
        return Item.builder().id(id).name("custom item").build();
    }
}
