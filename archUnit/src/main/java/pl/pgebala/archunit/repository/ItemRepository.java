package pl.pgebala.archunit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pgebala.archunit.entity.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {

}
