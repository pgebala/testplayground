package pl.pgebala.archunit.mapper;

import java.util.List;
import java.util.stream.Collectors;
import pl.pgebala.archunit.dto.ItemDto;
import pl.pgebala.archunit.entity.Item;

public class ItemMapper {

    public static ItemDto toDto(Item item) {
        return ItemDto.builder().id(item.getId()).name(item.getName()).build();
    }

    public static Item toEntity(ItemDto itemDto) {
        return Item.builder().id(itemDto.getId()).name(itemDto.getName()).build();
    }

    public static List<ItemDto> toDtos(List<Item> items) {
        return items.stream().map(ItemMapper::toDto).collect(Collectors.toList());
    }

    public static List<Item> toEntities(List<ItemDto> itemDtos) {
        return itemDtos.stream().map(ItemMapper::toEntity).collect(Collectors.toList());
    }
}
