package pl.pgebala.archunit.service.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pgebala.archunit.dto.ItemDto;
import pl.pgebala.archunit.mapper.ItemMapper;
import pl.pgebala.archunit.repository.CustomItemRepository;
import pl.pgebala.archunit.repository.ItemRepository;
import pl.pgebala.archunit.service.ItemService;
import pl.pgebala.archunit.util.ItemUtil;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemUtil itemUtil;
    private final ItemRepository itemRepository;
    private final CustomItemRepository customItemRepository;

    @Override
    @Transactional(readOnly = true)
    public List<ItemDto> findAll() {
        doSomething();
        return ItemMapper.toDtos(itemRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public ItemDto findOne(long id) {
        return ItemMapper.toDto(customItemRepository.getItem(id));
    }

    @Override
    @Transactional
    public ItemDto save(ItemDto item) {
        return ItemMapper.toDto(itemRepository.save(ItemMapper.toEntity(item)));
    }

    @Override
    @Transactional(readOnly = true)
    public long countAllItems() {
        return itemUtil.count(findAll());
    }

    private void doSomething() {
        System.out.println("SOMETHING");
    }
}
