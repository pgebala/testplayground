package pl.pgebala.archunit.service;

import java.util.List;
import pl.pgebala.archunit.dto.ItemDto;
import pl.pgebala.archunit.entity.Item;

public interface ItemService {

    List<ItemDto> findAll();

    ItemDto findOne(long id);

    ItemDto save(ItemDto item);

    long countAllItems();
}
